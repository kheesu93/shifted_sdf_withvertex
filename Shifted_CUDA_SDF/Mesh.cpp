#include "Mesh.h"

Mesh::Mesh()
{
	m_pVerts = NULL;
	m_pFaces = NULL;
	m_pCentroid = NULL;
	m_pTidArray = NULL;
	//m_pNeighFaces = NULL;
}

Mesh::~Mesh()
{
	SAFEDELETEARRAY(m_pVerts);
	SAFEDELETEARRAY(m_pFaces);
	SAFEDELETEARRAY(m_pCentroid);
	SAFEDELETEARRAY(m_pTidArray);
	//SAFEDELETEARRAY(m_pNeighFaces);
}

void Mesh::meshRead(std::string a)
{
	char myString[100];
	unsigned int dummy;

	FILE * fp = fopen(a.c_str(),"r");

	if(fp == NULL) perror("Error Opening File");
	else{
		fgets(myString, sizeof(myString), fp);
		cout << myString << endl;
		
		fscanf(fp, "%d %d %d\n", &m_numVerts, &m_numFaces, &dummy);
		printf("%d %d %d\n", m_numVerts, m_numFaces, dummy);

		m_pVerts = new Vertex[m_numVerts];
		m_pFaces = new Triangle[m_numFaces];
		
		m_gpuVerts = (gpuVertex *)malloc(m_numVerts * sizeof(gpuVertex));
		m_gpuFaces = (gpuTriangle *)malloc(m_numFaces * sizeof(gpuTriangle));

		for (unsigned int i = 0; i < m_numVerts; i++)
			fscanf(fp, "%f %f %f\n", &m_pVerts[i].x, &m_pVerts[i].y, &m_pVerts[i].z);
		

		for (unsigned int i = 0; i < m_numFaces; i++)
		{
			fscanf(fp, "%d %d %d %d", &dummy, &m_pFaces[i].m_ui[0], &m_pFaces[i].m_ui[1], &m_pFaces[i].m_ui[2]);
			m_gpuFaces[i].m_ui[0] = m_pFaces[i].m_ui[0];
			m_gpuFaces[i].m_ui[1] = m_pFaces[i].m_ui[1];
			m_gpuFaces[i].m_ui[2] = m_pFaces[i].m_ui[2];
		}

		fclose(fp);
	}

	Vertex m_vMax, m_vMin, length;
	float lmax;

	m_vMax.x = FLT_MIN;		
	m_vMax.y = FLT_MIN;		
	m_vMax.z = FLT_MIN;		

	m_vMin.x = FLT_MAX;		
	m_vMin.y = FLT_MAX;		
	m_vMin.z = FLT_MAX;		

	for (int i=0; i<m_numVerts; i++)
	{
		///////////////	X_MAX, X_MIN
		if (m_pVerts[i].x > m_vMax.x)
			m_vMax.x = m_pVerts[i].x;
		if (m_pVerts[i].x < m_vMin.x)
			m_vMin.x = m_pVerts[i].x;

		///////////////	Y_MAX, Y_MIN
		if (m_pVerts[i].y > m_vMax.y)
			m_vMax.y = m_pVerts[i].y;
		if (m_pVerts[i].y < m_vMin.y)
			m_vMin.y = m_pVerts[i].y;

		///////////////	Z_MAX, Z_MIN
		if (m_pVerts[i].z > m_vMax.z)
			m_vMax.z = m_pVerts[i].z;
		if (m_pVerts[i].z < m_vMin.z)
			m_vMin.z = m_pVerts[i].z;
	}	
	printf("\n\n<Before : BoundingBox>\nMax(%f, %f, %f)\nMin(%f, %f, %f)\n", m_vMax.x, m_vMax.y, m_vMax.z, m_vMin.x, m_vMin.y, m_vMin.z);

	length.x = m_vMax.x - m_vMin.x;
	length.y = m_vMax.y - m_vMin.y;
	length.z = m_vMax.z - m_vMin.z;
	cout << length.x << "  " << length.y << "  " << length.z << endl;

	lmax = max(max(length.x, length.y), length.z);
	cout << lmax <<endl;

	float xc, yc, zc;

	xc = (m_vMax.x + m_vMin.x) / 2.0f; 
	yc = (m_vMax.y + m_vMin.y) / 2.0f;
	zc = (m_vMax.z + m_vMin.z) / 2.0f;
	cout << "center : " << xc << " " <<  yc << " " <<  zc << endl;

	m_vMax.x = FLT_MIN;		
	m_vMax.y = FLT_MIN;		
	m_vMax.z = FLT_MIN;		

	m_vMin.x = FLT_MAX;		
	m_vMin.y = FLT_MAX;		
	m_vMin.z = FLT_MAX;	

	for (int i=0; i<m_numVerts; i++)
	{
		m_pVerts[i].x = (m_pVerts[i].x-xc)/lmax * 0.75 + 0.75/2.0f;
		m_pVerts[i].y = (m_pVerts[i].y-yc)/lmax * 0.75 + 0.75/2.0f;
		m_pVerts[i].z = (m_pVerts[i].z-zc)/lmax * 0.75 + 0.75/2.0f;

		m_gpuVerts[i].x = m_pVerts[i].x;
		m_gpuVerts[i].y = m_pVerts[i].y;
		m_gpuVerts[i].z = m_pVerts[i].z;

		///////////////	X_MAX, X_MIN
		if (m_gpuVerts[i].x > m_vMax.x)
			m_vMax.x = m_gpuVerts[i].x;
		if (m_gpuVerts[i].x < m_vMin.x)
			m_vMin.x = m_gpuVerts[i].x;

		///////////////	Y_MAX, Y_MIN
		if (m_gpuVerts[i].y > m_vMax.y)
			m_vMax.y = m_gpuVerts[i].y;
		if (m_gpuVerts[i].y < m_vMin.y)
			m_vMin.y = m_gpuVerts[i].y;

		///////////////	Z_MAX, Z_MIN
		if (m_gpuVerts[i].z > m_vMax.z)
			m_vMax.z = m_gpuVerts[i].z;
		if (m_gpuVerts[i].z < m_vMin.z)
			m_vMin.z = m_gpuVerts[i].z;
	}

	printf("\n\n<After : BoundingBox>\nMax(%f, %f, %f)\nMin(%f, %f, %f)\n", m_vMax.x, m_vMax.y, m_vMax.z, m_vMin.x, m_vMin.y, m_vMin.z);

	for(unsigned int tid=0; tid<m_numFaces; tid++)
	{
		for(int j=0; j<3; j++)
		{
			m_gpuFaces[tid].m_v[j].x = m_pVerts[m_pFaces[tid].m_ui[j]].x;
			m_gpuFaces[tid].m_v[j].y = m_pVerts[m_pFaces[tid].m_ui[j]].y;
			m_gpuFaces[tid].m_v[j].z = m_pVerts[m_pFaces[tid].m_ui[j]].z;
		}
	}

	m_pTidArray = new unsigned int[m_numFaces];

	for(int i=0; i<m_numFaces; i++)
	{
		Triangle * pTris = &m_pFaces[i];
		pTris->m_Tid = i;
		m_pTidArray[i] = m_pFaces[i].m_Tid;
		
		for(int k=0; k<3; k++)
			pTris->v(k, m_pVerts)->AddNeighbor(pTris);
	
	}	
	getTcentroid(m_pTidArray);

	/////////// NeighborFace를 <vector>type으로 저장
	for(int j=0; j<m_numVerts; j++)
	{
		int numNb = m_pVerts[j].NbPolygonNeighbor();
		for(int k=0; k<numNb; k++)
			m_vNeighFaces.push_back(m_pVerts[j].GetPolygonNeighbor(k)->m_Tid);
	} 


	/////////// <vector>type의 NeighborFace를 --> array로 다시 저장!
	m_NeighFcount = m_vNeighFaces.size();
	m_pNeighFaces = new unsigned int[m_NeighFcount];
	for(int i=0; i<m_NeighFcount; i++)
		m_pNeighFaces[i] = m_vNeighFaces.at(i);


	/////////// 각 Neighbor Array의 시작하는 위치 인덱스 저장
	m_pVerts[0].vNfid = 0;
	for(int i=1; i<m_numVerts; i++)
	{
		m_pVerts[i].vNfid = m_pVerts[i-1].vNfid + m_pVerts[i-1].NbPolygonNeighbor();
		m_gpuVerts[i-1].gpuNfid = m_pVerts[i-1].vNfid;
	}
}

Vertex * Mesh::getTcentroid(unsigned int *  Tid)
{
	m_pCentroid = new Vertex[m_numFaces];
	
	for(int i=0; i<m_numFaces; i++)
	{
		unsigned int id = Tid[i];
		m_pCentroid[id].x = 0.0f;
		m_pCentroid[id].y = 0.0f;
		m_pCentroid[id].z = 0.0f;

		for(int j=0; j<3; j++)
		{
			m_pCentroid[id].x += m_pVerts[m_pFaces[id].m_ui[j]].x;
			m_pCentroid[id].y += m_pVerts[m_pFaces[id].m_ui[j]].y;
			m_pCentroid[id].z += m_pVerts[m_pFaces[id].m_ui[j]].z;
		}
		m_pCentroid[id].x /= 3.0f;
		m_pCentroid[id].y /= 3.0f;
		m_pCentroid[id].z /= 3.0f;
	}
	return m_pCentroid;
}

Vertex  Mesh:: VertexAdjacency(Vertex * pVert)
{
	float Nx=0;
	float Ny=0;
	float Nz=0;

	Vertex v3VertNormal;
	
	int NbPolygonNeighbor = pVert->NbPolygonNeighbor();
	
	for(int k=0; k<NbPolygonNeighbor; k++)
	{
		Triangle * pNFaces = pVert->GetPolygonNeighbor(k);
		Nx += pNFaces->m_normals.x;
		Ny += pNFaces->m_normals.y;
		Nz += pNFaces->m_normals.z;
	}

	///////// A Triangle's one vertex normal x,y,z of three vertices
	pVert->nx = Nx /(float)NbPolygonNeighbor;
	pVert->ny = Ny /(float)NbPolygonNeighbor;
	pVert->nz = Nz /(float)NbPolygonNeighbor;

	v3VertNormal.x = pVert->nx;
	v3VertNormal.y = pVert->ny;
	v3VertNormal.z = pVert->nz;

	return v3VertNormal;
}
