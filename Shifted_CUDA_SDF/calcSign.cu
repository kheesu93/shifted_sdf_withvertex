#include "calcSign.cuh"

__global__ void calcSign(
	unsigned int		num_queries,
	unsigned int		num_faces,
	gpuVertex		*	d_queries,
	unsigned int	*	d_outFinIdx,
	gpuVertex		*	d_tpFaceNorm,
	gpuVertex		*	d_vertNorm,
	gpuVertex		*	d_data,
	float			*	d_sign)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_queries <= tid)
		return;

	unsigned int minIdx = d_outFinIdx[tid];
	gpuVertex	query = d_queries[tid];
	float sign;
	gpuVertex vec = query - d_data[minIdx];
	gpuVertex normal;

	if(minIdx < num_faces)
		normal = d_tpFaceNorm[minIdx];
	else
		normal = d_vertNorm[tid-num_faces];

	float innerPro = normal.x * vec.x + normal.y * vec.y + normal.z * vec.z;
	
	if(innerPro > 0)
		sign = 1.0f;
	else
		sign = -1.0f;
	//float sign = innerPro / fabs(innerPro);

	d_sign[tid] = sign;
}