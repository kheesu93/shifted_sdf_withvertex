#include <cuda.h>
#include <cuda_runtime.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <cub/cub.cuh>
#include <cub/device/device_radix_sort.cuh>

#include "Mesh.h"
#include "cubShiftedSort.cuh"
#include "calcFaceNormal.cuh"
#include "calcVertexNormal.cuh"
#include "calcMinDistance.cuh"
#include "centroidDistInfo.cuh"
#include "calcSign.cuh"
#include "Stopwatch.h"

Mesh	*	g_ptriMesh;

int main()
{
	CStopWatch timer;

	std::string git_dir(getenv("GITDIR"));
	std::string mesh_path = git_dir + "/mesh/spring.off";
	std::string query_path = git_dir + "/mesh/query_2d_256x256.txt";
	std::string result_path = git_dir + "/mesh/shifted/result_dist_spring.txt";
	std::string L2norm_path = git_dir + "/mesh/reduction/result_spring.txt";

	timer.Start();

	g_ptriMesh = new Mesh();
	g_ptriMesh->meshRead(mesh_path);

	unsigned int			num_queries;
	unsigned int			num_verts	 = g_ptriMesh->m_numVerts;
	unsigned int			num_faces	 = g_ptriMesh->m_numFaces;
	unsigned int			num_NFcount	 = g_ptriMesh->m_NeighFcount;
	
	gpuVertex		*		h_vpVerts	 = g_ptriMesh->m_gpuVerts;
	gpuTriangle		*		h_tpFaces	 = g_ptriMesh->m_gpuFaces;
	unsigned int	*		h_uipTidArr	 = g_ptriMesh->m_pTidArray;
	Vertex			*		h_vpCentroid = g_ptriMesh->getTcentroid(h_uipTidArr);
	

	FILE			*		fp = fopen(query_path.c_str(),"r");
	fscanf(fp, "%d\n", &num_queries);

	vector <Point>			data(num_faces + num_verts);
	vector <Point>			queries(num_queries);

	gpuVertex		*		h_data		= new gpuVertex[num_faces + num_verts];
	gpuVertex		*		h_queries	= new gpuVertex[num_queries];


	for(unsigned int i = 0; i < num_queries; i++)
	{
		fscanf(fp, "%f\t%f\t%f\n", &queries[i].coords[0], &queries[i].coords[1], &queries[i].coords[2]);
		h_queries[i].x = queries[i].coords[0];
		h_queries[i].y = queries[i].coords[1];
		h_queries[i].z = queries[i].coords[2];
	}
	fclose(fp);


	for(unsigned int k=0; k< num_faces; k++)
	{
		data[k].coords[0] = h_vpCentroid[k].x;
		data[k].coords[1] = h_vpCentroid[k].y;
		data[k].coords[2] = h_vpCentroid[k].z;

		h_data[k].x = data[k].coords[0];
		h_data[k].y = data[k].coords[1];
		h_data[k].z = data[k].coords[2];
	}

	for(unsigned int k=0; k< num_verts; k++)
	{
		data[k+num_faces].coords[0] = h_vpVerts[k].x;
		data[k+num_faces].coords[1] = h_vpVerts[k].y;
		data[k+num_faces].coords[2] = h_vpVerts[k].z;

		h_data[k+num_faces].x = data[k+num_faces].coords[0];
		h_data[k+num_faces].y = data[k+num_faces].coords[1];
		h_data[k+num_faces].z = data[k+num_faces].coords[2];
	}

	unsigned int	*		pIndices = new unsigned int[queries.size()*NUM_NEIGH];
	float			*		pDistances = new float[queries.size()*NUM_NEIGH];

	unsigned int			blocksize	 = 32;
	dim3					block;
	dim3					grid;
	block.x			=		blocksize;
	

	gpuVertex		*		h_tpNorm = new gpuVertex[num_faces];
	unsigned int	*		h_NeighFaces = g_ptriMesh->m_pNeighFaces;
	gpuVertex		*		h_vertNorm = new gpuVertex[num_verts];
	float			*		h_finDist = new float[num_queries];
	float			*		h_sign = new float[num_queries];


	gpuVertex		*		d_Verts;
	gpuTriangle		*		d_Faces;
	unsigned int	*		d_NeighFaces;
	gpuVertex		*		d_tpFaceNorm;
	gpuVertex		*		d_NeighNorm;
	gpuVertex		*		d_vertNorm;
	gpuVertex		*		d_data;
	gpuVertex		*		d_queries;
	unsigned int	*		d_indices;
	float			*		d_dists;
	float			*		d_outDList;
	unsigned int	*		d_outDIdx;
	float			*		d_outFinDist;
	unsigned int	*		d_outFinIdx;
	float			*		d_outSortedDist;
	unsigned int	*		d_outSortedIdx;
	float			*		d_sign;


	
	cudaMalloc(&d_Verts, sizeof(gpuVertex) * num_verts);
	cudaMalloc(&d_Faces, sizeof(gpuTriangle) * num_faces);
	cudaMalloc(&d_NeighFaces, sizeof(unsigned int) * num_NFcount);
	cudaMalloc(&d_tpFaceNorm, sizeof(gpuVertex) * num_faces);
	cudaMalloc(&d_NeighNorm, sizeof(gpuVertex) * num_NFcount);
	cudaMalloc(&d_vertNorm, sizeof(gpuVertex) * num_verts);
	cudaMalloc(&d_data, sizeof(gpuVertex) * (num_faces+num_verts));
	cudaMalloc(&d_queries, sizeof(gpuVertex) * num_queries);
	cudaMalloc(&d_indices, sizeof(unsigned int) * num_queries*NUM_NEIGH);
	cudaMalloc(&d_dists, sizeof(float)*num_queries * NUM_NEIGH);
	cudaMalloc(&d_outDList, sizeof(float)*num_queries * NUM_NEIGH);
	cudaMalloc(&d_outDIdx, sizeof(unsigned int)*num_queries * NUM_NEIGH);
	cudaMalloc(&d_outFinDist, sizeof(float) * num_queries);
	cudaMalloc(&d_outFinIdx, sizeof(unsigned int) *num_queries);
	cudaMalloc(&d_outSortedDist, sizeof(float) * num_queries*NUM_NEIGH);
	cudaMalloc(&d_outSortedIdx, sizeof(unsigned int) * num_queries*NUM_NEIGH);
	cudaMalloc(&d_sign, sizeof(float) * num_queries);

	cudaMemcpy(d_Verts, h_vpVerts, sizeof(gpuVertex) * num_verts, cudaMemcpyHostToDevice);
	cudaMemcpy(d_Faces, h_tpFaces, sizeof(gpuTriangle) * num_faces, cudaMemcpyHostToDevice);
	cudaMemcpy(d_NeighFaces, h_NeighFaces, sizeof(unsigned int) * num_NFcount, cudaMemcpyHostToDevice);
	cudaMemcpy(d_data, h_data, sizeof(gpuVertex) * (num_faces+num_verts), cudaMemcpyHostToDevice);
	cudaMemcpy(d_queries, h_queries, sizeof(gpuVertex) * num_queries, cudaMemcpyHostToDevice);

	
	grid.x = (num_faces + block.x - 1) / block.x;
	calcFaceNormal<<<grid, block>>>(d_Verts, d_Faces, d_tpFaceNorm, num_faces);
	cudaMemcpy(h_tpNorm, d_tpFaceNorm, sizeof(gpuVertex) * num_faces, cudaMemcpyDeviceToHost);
	for(unsigned int i=0; i<num_faces; i++)
		h_tpFaces[i].m_normals = h_tpNorm[i];


	cudaMemcpy(d_Faces, h_tpFaces, sizeof(gpuTriangle) * num_faces, cudaMemcpyHostToDevice);
	grid.x = (num_NFcount + block.x - 1) / block.x;
	calcVertexNormal<<<grid, block>>>(d_NeighFaces, num_NFcount, d_Faces, d_NeighNorm);
	
	grid.x = (num_verts + block.x - 1) / block.x;
	calcVertexNormal2<<<grid, block>>>(d_NeighNorm, d_Verts, num_verts, d_vertNorm, num_NFcount);
	cudaMemcpy(h_vertNorm, d_vertNorm, sizeof(gpuVertex) * num_verts, cudaMemcpyDeviceToHost);


	kANNShiftedSortNoCache(data, queries, pIndices, pDistances, data.size(), queries.size());	
	cudaMemcpy(d_indices, pIndices, sizeof(unsigned int) * num_queries*NUM_NEIGH, cudaMemcpyHostToDevice);
	cudaMemcpy(d_dists, pDistances, sizeof(float) * num_queries*NUM_NEIGH, cudaMemcpyHostToDevice);


	grid.x = (num_queries*NUM_NEIGH + block.x - 1) / block.x;
	centroidDistInfo<<<grid, block>>>(num_queries, d_indices, d_dists, num_verts, num_faces, d_Faces, d_outDList, d_queries, d_outDIdx);
	

	unsigned int hd = 0u;		// this bit pattern is used for warp scan for 2k-sub warps.
	unsigned int sift = 1u;	
	while(sift != 0u)
	{
		hd = hd | sift;
		sift =  sift << NUM_NEIGH;
	}

	calcMinDistance<<<grid, block>>>(num_queries, d_outDIdx, d_outDList, hd, d_outSortedDist, d_outSortedIdx, d_outFinDist, d_outFinIdx);
	cudaMemcpy(h_finDist, d_outFinDist, sizeof(float) * num_queries , cudaMemcpyDeviceToHost);


	grid.x = (num_queries + block.x - 1) / block.x;
	calcSign<<<grid, block>>>(num_queries, num_faces, d_queries, d_outFinIdx, d_tpFaceNorm, d_vertNorm, d_data, d_sign);
	cudaMemcpy(h_sign, d_sign, sizeof(float) * num_queries, cudaMemcpyDeviceToHost);
	
	
	float * reductionDist = new float[num_queries];
	float result = 0.0f;


	FILE	*	fpL2 = fopen(L2norm_path.c_str(),"r");
	fscanf(fpL2, "%d\n", &num_queries);
	for(unsigned int i = 0; i < num_queries; i++)
		fscanf(fpL2, "%f\n", &reductionDist[i]);
	fclose(fpL2);

	
	FILE	*	fp3 = fopen(result_path.c_str(),"w");
	fprintf(fp3, "%d\n", num_queries);
	for(unsigned int i=0; i<num_queries; i++){
		//fprintf(fp3, "%f\n", h_sign[i]*h_finDist[i]);
		fprintf(fp3, "%f\n", h_finDist[i]);
		result += pow(reductionDist[i]-h_finDist[i], 2);
	}
	result = sqrt(result);
	cout << "L2 Norm : " << result << endl;
	fclose(fp3);

	timer.Stop();
	timer.PrintAccumTime();


	delete [] h_vpVerts;
	delete [] h_tpFaces;
	delete [] h_NeighFaces;
	delete [] h_uipTidArr;
	delete [] h_vpCentroid;
	delete [] h_data;
	delete [] h_queries;
	delete [] pIndices;
	delete [] pDistances;
	delete [] h_tpNorm;
	delete [] h_vertNorm;
	delete [] h_finDist;
	delete [] h_sign;
	delete [] reductionDist;

	cudaFree(d_Verts);
	cudaFree(d_Faces);
	cudaFree(d_NeighFaces);
	cudaFree(d_tpFaceNorm);
	cudaFree(d_NeighNorm);
	cudaFree(d_vertNorm);
	cudaFree(d_data);
	cudaFree(d_queries);
	cudaFree(d_indices);
	cudaFree(d_dists);
	cudaFree(d_outDList);
	cudaFree(d_outDIdx);
	cudaFree(d_outFinDist);
	cudaFree(d_outFinIdx);
	cudaFree(d_outSortedDist);
	cudaFree(d_outSortedIdx);
	cudaFree(d_sign);
}