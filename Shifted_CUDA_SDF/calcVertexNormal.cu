#include "calcVertexNormal.cuh"

__global__ void calcVertexNormal(
	unsigned int	*	neighFaces, 
	unsigned int		num_Neigh, 
	gpuTriangle		*	pfaces, 
	gpuVertex		*	d_NeighNorm)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_Neigh <= tid)
		return;

	unsigned int vNeighFaces = neighFaces[tid];  
	gpuTriangle NeighTrianfgle = pfaces[vNeighFaces];
	gpuVertex NeighNorm = NeighTrianfgle.m_normals;
	d_NeighNorm[tid] = NeighNorm;
}


__global__ void calcVertexNormal2(
	gpuVertex		*	d_NeighNorm, 
	gpuVertex		*	pVerts, 
	unsigned int		num_verts,
	gpuVertex		*	d_vertNorm,
	unsigned int		num_Neigh)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_verts <= tid)
		return;

	gpuVertex		vertNorm;
	unsigned int	idx;
	unsigned int	Ncount;

	vertNorm.x = 0;
	vertNorm.y = 0;
	vertNorm.z = 0;

	Ncount = pVerts[tid+1].gpuNfid - pVerts[tid].gpuNfid;
	idx = pVerts[tid].gpuNfid;
	
	for(unsigned int j=0; j<Ncount; j++)
	{
		if(idx+j > num_Neigh)
			return;

		vertNorm.x += d_NeighNorm[idx+j].x;
		vertNorm.y += d_NeighNorm[idx+j].y;
		vertNorm.z += d_NeighNorm[idx+j].z;
	}

	vertNorm.x /= (float)Ncount;
	vertNorm.y /= (float)Ncount;
	vertNorm.z /= (float)Ncount;

	d_vertNorm[tid] = vertNorm;
}