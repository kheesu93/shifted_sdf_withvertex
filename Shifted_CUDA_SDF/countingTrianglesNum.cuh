#ifndef ___COUNTINGTRIANGLENUM___
#define ___COUNTINGTRIANGLENUM___

#include <cuda.h>
#include <cuda_runtime.h>

#include "Vertex.h"
#define NUM_NEIGH				8

__global__ void countingTrianglesNum(
	unsigned int	*	d_indices,
	gpuVertex		*	pVerts,
	unsigned int		num_queries,
	unsigned int		num_verts,
	unsigned int	*	d_outNumT);

#endif