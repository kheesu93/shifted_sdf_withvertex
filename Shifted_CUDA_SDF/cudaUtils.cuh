//****************************************************************************************
//	cuUtils.cuh
// Taejung Park (taejung.park at gmail.com)
// Created : Aug 22 2016
// Modified :
// 
//
// Description : Utility entries to help cuda programming
//				 
//****************************************************************************************

#ifndef ___CUDAUTILS__CUH_____________________________
#define ___CUDAUTILS__CUH_____________________________


#include <vector_types.h>

dim3 get1DGridSize(unsigned int nElem, unsigned int threadsPerBlock)
{
	dim3 grid;
	grid.x = (nElem + threadsPerBlock - 1)/threadsPerBlock;
	return grid;
}

#endif