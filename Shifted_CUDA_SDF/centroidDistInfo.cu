#include "centroidDistInfo.cuh"

__global__ void centroidDistInfo(
	unsigned int		num_queries,
	unsigned int	*	d_indices,
	float			*	d_dist,
	unsigned int		num_verts,
	unsigned int		num_faces,
	gpuTriangle		*	pfaces,
	float			*	d_outDList,
	gpuVertex		*	d_queries,
	unsigned int	*	d_outDIdx)
{
	unsigned int	tid = blockIdx.x * blockDim.x + threadIdx.x;
	int				region = -1;

	if(num_queries*NUM_NEIGH <= tid)
		return;


	unsigned int	idx = d_indices[tid];
	unsigned int	qIdx = tid/NUM_NEIGH;
	float			Distance;

	if(idx < num_faces)
	{

		float T0x = pfaces[idx].m_v[0].x; float T0y = pfaces[idx].m_v[0].y; float T0z = pfaces[idx].m_v[0].z;
		float T1x = pfaces[idx].m_v[1].x; float T1y = pfaces[idx].m_v[1].y; float T1z = pfaces[idx].m_v[1].z;
		float T2x = pfaces[idx].m_v[2].x; float T2y = pfaces[idx].m_v[2].y; float T2z = pfaces[idx].m_v[2].z;


		float kDiffx = T0x - d_queries[tid/NUM_NEIGH].x;
		float kDiffy = T0y - d_queries[tid/NUM_NEIGH].y;
		float kDiffz = T0z - d_queries[tid/NUM_NEIGH].z;
		//MgcVector3 kDiff = rkTri.Origin() - rkPoint;

		float fA00 =  (T1x - T0x) * (T1x - T0x) + (T1y -  T0y) * (T1y - T0y) + (T1z - T0z) * (T1z - T0z);
		float fA01 = (T1x - T0x) * (T2x - T0x) + (T1y - T0y) * (T2y - T0y) + (T1z - T0z) * (T2z - T0z);
		float fA11 = (T2x - T0x) * (T2x - T0x) + (T2y - T0y) * (T2y - T0y) + (T2z - T0z) * (T2z - T0z);
		float fB0 = kDiffx * (T1x -  T0x) + kDiffy * (T1y -  T0y) + kDiffz * (T1z -  T0z);
		float fB1 = kDiffx * (T2x -  T0x) + kDiffy * (T2y -  T0y) + kDiffz * (T2z -  T0z);
		float fC = kDiffx * kDiffx + kDiffy * kDiffy + kDiffz * kDiffz;

		float fDet = fabs(fA00 * fA11 - fA01 * fA01);
		float fS = fA01 * fB1 - fA11 * fB0;
		float fT = fA01 * fB0 - fA00 * fB1;
		float fSqrDist;

		if ( fS + fT <= fDet )
		{
			if ( fS < 0.0 )
			{	          
				if ( fT < 0.0 )  // region 4
				{
					region = 4;
					if ( fB0 < 0.0 )
					{
						fT = 0.0;
						if ( -fB0 >= fA00 )
						{
							fS = 1.0;
							fSqrDist = fA00+2.0*fB0+fC;
						}
						else
						{
							fS = -fB0/fA00;
							fSqrDist = fB0*fS+fC;
						}
					}
					else
					{
						fS = 0.0;
						if ( fB1 >= 0.0 )
						{
							fT = 0.0;
							fSqrDist = fC;
						}
						else if ( -fB1 >= fA11 )
						{
							fT = 1.0;
							fSqrDist = fA11+2.0*fB1+fC;			    
						}
						else
						{
							fT = -fB1/fA11;
							fSqrDist = fB1*fT+fC;
						}
					}
				}
				else  // region 3
				{
					region = 3;
					fS = 0.0;
					if ( fB1 >= 0.0 )
					{
						fT = 0.0;
						fSqrDist = fC;
					}
					else if ( -fB1 >= fA11 )
					{
						fT = 1;
						fSqrDist = fA11+2.0*fB1+fC;
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
			else if ( fT < 0.0 )  // region 5
			{
				region = 5;

				fT = 0.0;
				if ( fB0 >= 0.0 )
				{
					fS = 0.0;
					fSqrDist = fC;
				}
				else if ( -fB0 >= fA00 )
				{
					fS = 1.0;
					fSqrDist = fA00+2.0*fB0+fC;
				}
				else
				{
					fS = -fB0/fA00;
					fSqrDist = fB0*fS+fC;
				}
			}
			else  // region 0
			{
				region = 0;
				// minimum at interior point
				float fInvDet = 1.0/fDet;
				fS *= fInvDet;
				fT *= fInvDet;
				fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
					fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
			}
		}
		else
		{
			float fTmp0, fTmp1, fNumer, fDenom;

			if ( fS < 0.0 )  // region 2
			{
				region = 2;
				fTmp0 = fA01 + fB0;
				fTmp1 = fA11 + fB1;
				if ( fTmp1 > fTmp0 )
				{
					fNumer = fTmp1 - fTmp0;
					fDenom = fA00-2.0*fA01+fA11;
					if ( fNumer >= fDenom )
					{
						fS = 1.0;
						fT = 0.0;
						fSqrDist = fA00+2.0*fB0+fC;
					}
					else
					{
						fS = fNumer/fDenom;
						fT = 1.0 - fS;
						fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
							fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
					}
				}
				else
				{
					fS = 0.0;
					if ( fTmp1 <= 0.0 )
					{
						fT = 1.0;
						fSqrDist = fA11+2.0*fB1+fC;
					}
					else if ( fB1 >= 0.0 )
					{
						fT = 0.0;
						fSqrDist = fC;
					}
					else
					{
						fT = -fB1/fA11;
						fSqrDist = fB1*fT+fC;
					}
				}
			}
			else if ( fT < 0.0 )  // region 6
			{
				region = 6;
				fTmp0 = fA01 + fB1;
				fTmp1 = fA00 + fB0;
				if ( fTmp1 > fTmp0 )
				{
					fNumer = fTmp1 - fTmp0;
					fDenom = fA00-2.0*fA01+fA11;
					if ( fNumer >= fDenom )
					{
						fT = 1.0;
						fS = 0.0;
						fSqrDist = fA11+2.0*fB1+fC;
					}
					else
					{
						fT = fNumer/fDenom;
						fS = 1.0 - fT;
						fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
							fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
					}
				}
				else
				{
					fT = 0.0;
					if ( fTmp1 <= 0.0 )
					{
						fS = 1.0;
						fSqrDist = fA00+2.0*fB0+fC;
					}
					else if ( fB0 >= 0.0 )
					{
						fS = 0.0;
						fSqrDist = fC;
					}
					else
					{
						fS = -fB0/fA00;
						fSqrDist = fB0*fS+fC;
					}
				}
			}
			else  // region 1
			{
				region = 1;

				fNumer = fA11 + fB1 - fA01 - fB0;
				if ( fNumer <= 0.0 )
				{
					fS = 0.0;
					fT = 1.0;
					fSqrDist = fA11+2.0*fB1+fC;
				}
				else
				{
					fDenom = fA00-2.0*fA01+fA11;
					if ( fNumer >= fDenom )
					{
						fS = 1.0;
						fT = 0.0;
						fSqrDist = fA00+2.0*fB0+fC;
					}
					else
					{
						fS = fNumer/fDenom;
						fT = 1.0 - fS;
						fSqrDist = fS*(fA00*fS+fA01*fT+2.0*fB0) +
							fT*(fA01*fS+fA11*fT+2.0*fB1)+fC;
					}
				}
			}
		}
		Distance = fabs(fSqrDist);


	}
	else
		Distance = d_dist[tid];


	//	result.region = (char)region;
	d_outDList[tid] = Distance;
	d_outDIdx[tid] = idx;
}