#include "calcInnerproductDistance.cuh"

__global__ void calcInnerproductDistance(
	gpuVertex		*	d_data, 
	gpuVertex		*	d_queries,
	unsigned int	*	d_indices,
	gpuTriangle		*	d_Faces,
	unsigned int		num_verts,
	unsigned int		num_queries,
	gpuVertex		*	d_vertNorm,
	float			*	d_innerDistance)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_queries * NUM_NEIGH <= tid)
		return;

	unsigned int indices = d_indices[tid];
	float innerproduct;

	if(indices < num_verts)
	{
		for(unsigned int i=0; i<num_queries; i++)
		{
			innerproduct = (d_vertNorm[indices].x * (d_queries[i].x - d_data[indices].x)) + (d_vertNorm[indices].y * (d_queries[i].y - d_data[indices].y)) + (d_vertNorm[indices].z * (d_queries[i].z - d_data[indices].z));
		}
	}
	else
	{
		unsigned int data_face_part = indices - num_verts;
		for(unsigned int i=0; i<num_queries; i++)
		{
			innerproduct = (d_Faces[data_face_part].m_normals.x * (d_queries[i].x - d_data[data_face_part].x)) + (d_Faces[data_face_part].m_normals.y * (d_queries[i].y - d_data[data_face_part].y)) + (d_Faces[data_face_part].m_normals.z * (d_queries[i].z - d_data[data_face_part].z));
		}
	}

	d_innerDistance[tid] = innerproduct;
}