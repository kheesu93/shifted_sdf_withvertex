#include "countingTrianglesNum.cuh"

__global__ void countingTrianglesNum(
	unsigned int	*	d_indices,
	gpuVertex		*	pVerts,
	unsigned int		num_queries,
	unsigned int		num_verts,
	unsigned int	*	d_outNumT)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_queries*NUM_NEIGH <= tid)
		return;

	unsigned int idx = d_indices[tid];
	unsigned int output;


	//if(idx < num_verts)
	//	output = pVerts[idx+1].gpuNfid - pVerts[idx].gpuNfid;
	//else
		output = 1;

	d_outNumT[tid] = output;
}