#include "calcFaceNormal.cuh"


__global__ void calcFaceNormal(
	gpuVertex		*	pVerts, 
	gpuTriangle		*	pFaces, 
	gpuVertex		*	pNorm, 
	unsigned int		numFaces)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(numFaces <= tid)
		return;

	gpuVertex	edge1;
	gpuVertex	edge2;

	gpuTriangle myFaces = pFaces[tid];
	edge2.x = pVerts[myFaces.m_ui[1]].x - pVerts[myFaces.m_ui[0]].x;
	edge2.y = pVerts[myFaces.m_ui[1]].y - pVerts[myFaces.m_ui[0]].y;
	edge2.z = pVerts[myFaces.m_ui[1]].z - pVerts[myFaces.m_ui[0]].z;

	edge1.x = pVerts[myFaces.m_ui[2]].x - pVerts[myFaces.m_ui[0]].x;
	edge1.y = pVerts[myFaces.m_ui[2]].y - pVerts[myFaces.m_ui[0]].y;
	edge1.z = pVerts[myFaces.m_ui[2]].z - pVerts[myFaces.m_ui[0]].z;

	myFaces.m_normals.x = (edge2.y * edge1.z) - (edge2.z * edge1.y);
	myFaces.m_normals.y = (edge2.z * edge1.x) - (edge2.x * edge1.z);
	myFaces.m_normals.z = (edge2.x * edge1.y) - (edge2.y * edge1.x);

	float length = sqrt((myFaces.m_normals.x * myFaces.m_normals.x) + (myFaces.m_normals.y * myFaces.m_normals.y) + (myFaces.m_normals.z * myFaces.m_normals.z));


	myFaces.m_normals.x/= length;
	myFaces.m_normals.y/= length;
	myFaces.m_normals.z/= length;

	pNorm[tid] = myFaces.m_normals;
}