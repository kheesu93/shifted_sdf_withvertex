#ifndef ___CALC___SIGN___
#define ___CALC___SIGN___

#include <cuda.h>
#include <cuda_runtime.h>

#include "Vertex.h"

__global__ void calcSign(
	unsigned int		num_queries,
	unsigned int		num_faces,
	gpuVertex		*	d_queries,
	unsigned int	*	d_outFinIdx,
	gpuVertex		*	d_tpFaceNorm,
	gpuVertex		*	d_vertNorm,
	gpuVertex		*	d_data,
	float			*	d_sign);

#endif