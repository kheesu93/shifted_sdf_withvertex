#ifndef ___CALCVERTEXNORMAL___CUH___
#define ___CALCVERTEXNORMAL___CUH___

#include <cuda.h>
#include <cuda_runtime.h>

#include "Mesh.h"

__global__ void calcVertexNormal(
	unsigned int	*	neighFaces, 
	unsigned int		num_Neigh, 
	gpuTriangle		*	pfaces, 
	gpuVertex		*	d_NeighNorm);

__global__ void calcVertexNormal2(
	gpuVertex		*	d_NeighNorm,
	gpuVertex		*	pVerts, 
	unsigned int		num_verts,
	gpuVertex		*	d_vertNorm,
	unsigned int		num_Neigh);

#endif