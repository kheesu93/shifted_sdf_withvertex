#ifndef __STOPWATCH_H__
#define __STOPWATCH_H__
#include < windows.h >
#include < stdio.h >
#pragma once
#define CHECK_TIME_START QueryPerformanceFrequency ((_LARGE_INTEGER*)&freq); QueryPerformanceCounter((_LARGE_INTEGER*)&start)
#define CHECK_TIME_END(a) QueryPerformanceCounter((_LARGE_INTEGER*)&end); a=(float)((float) (end - start)/freq)

class CStopWatch
{
public :
	int nCount;
	__int64 start, freq, end;
	double	resultTime;
	double	m_dAccumTime;
public:
	CStopWatch(void);
	~CStopWatch(void);

	void		Start(void) 
	{
		resultTime=0;
		CHECK_TIME_START;
	}

	void		PrintAccumTime(void) 
	{
		printf("\nAccum Time : accum time = %lf, count = %d, actual time = %lf(sec)\n", m_dAccumTime, nCount, resultTime);
	}

	double		Stop(void)
	{
		CHECK_TIME_END(resultTime);
		m_dAccumTime += resultTime;
		nCount++;
		return resultTime;
	}
};

#endif