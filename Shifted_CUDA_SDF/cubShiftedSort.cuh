//****************************************************************************************
//	cubShiftedSort.cuh
// Taejung Park (taejung.park at gmail.com)
// Created : March 6 2016
// Modified 1 :
// 
//
// Description : CUDA kernel for shifted sort
//				 
//****************************************************************************************


#ifndef ___CUBSHIFTEDSORT____H__________
#define ___CUBSHIFTEDSORT____H__________

#define TRUNC_BIT_RESOLUTION	15
#define WARP_SIZE				32
#define G_BLOCKSIZE				32
#define NUM_NEIGH				8

#include <stdint.h>
#include "tjvector_types.h"
#include "morton64.cuh"
//#include "KDtree.h"
#include <vector>

#define KDTREE_DIM 3 // data dimensions

struct Point
{
    float coords[KDTREE_DIM];
};


float distance(Point * pA, Point * pB);

__global__	void flagDataPoints(unsigned int * d_pMixedIndices, unsigned int * d_pOut_flags, unsigned int numDataPoints, unsigned int num);
__global__	void computeMortoncodes(float *d_x, float *d_y, float *d_z, unsigned long long * d_shiftedMorton, float offset, unsigned int numDataPoints, unsigned int numQueryPoints);
__global__	void storeNNCandidates(
	unsigned int * d_partitionedMixedIndices, 
	unsigned int * d_partitioneExSum, 
	unsigned long long * d_partitioned_MC, 
	unsigned int * d_2kNNIdx,  
	unsigned int * d_2kNNDist, 
	unsigned long long * d_locally_sorted_MC, 
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors);



__global__ void storeNNCandidatesEuclidean(
	unsigned int * d_partitionedMixedIndices, 
	unsigned int * d_partitioneExSum, 
	unsigned long long * d_partitioned_MC, 
	unsigned int * d_2kNNIdx,  
	float * d_2kNNDist, 
	unsigned long long * d_locally_sorted_MC, 
	float * px, float * py, float * pz,
	unsigned int numDataPoints, 
	unsigned int numQueryPoints, 
	unsigned int numNeighbors);

__host__ __device__ unsigned int getTruncatedUIntSqrDistance(unsigned long long a, unsigned long long b, unsigned int numBits);
__host__ __device__ float getFloatSqrDistance(unsigned long long a, unsigned long long b);



extern "C"
	void kANNShiftedSortNoCache(std::vector <Point> DataPoints, 
	std::vector <Point> QueryPoints, 
	unsigned int * outIndices,
	float * outDistance,
	size_t numDataPoints, 
	size_t numQueryPoints);

extern "C"
	void testCUB_WarpScan(void);


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
	if (code != cudaSuccess) 
	{
	//	fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		printf("GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}
void selectTheBestGPU(void);

#endif
