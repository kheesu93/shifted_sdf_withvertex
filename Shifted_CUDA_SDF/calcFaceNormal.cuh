#ifndef ____CALCFACENORMAL____CUH____
#define ____CALCFACENORMAL____CUH____

#include <cuda.h>
#include <cuda_runtime.h>
#include <math.h>

#include "Vertex.h"
#include "Triangle.h"

__global__ void calcFaceNormal(
	gpuVertex		*	pVerts, 
	gpuTriangle		*	pFaces, 
	gpuVertex		*	pNorm, 
	unsigned int		numFaces);



#endif