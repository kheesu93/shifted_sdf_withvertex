
//****************************************************************************************
//	cudaWatch.cuh
// Taejung Park (taejung.park at gmail.com)
// Created : May 17, 2016
// Modified :
// 
//
// Description : CUDA time measure class 
//  (based on Chap. 6, John Cheng et al. Professional CUDA C Programming)
//				 
//****************************************************************************************


#ifndef ______________CUDAWATCH______________CUH________________
#define ______________CUDAWATCH______________CUH________________
#include <cuda_runtime_api.h>
#include <stdio.h>
class cudaWatch
{
	cudaEvent_t m_start, m_stop;
	float m_time;
	float m_total_time;
public:
	cudaWatch()
	{		
		cudaEventCreate(&m_start);
		cudaEventCreate(&m_stop);
		m_time = 0.0f;
		m_total_time = 0.0f;
	}

	~cudaWatch()
	{
		cudaEventDestroy(m_start);
		cudaEventDestroy(m_stop);
	}

	void start()
	{
		cudaEventRecord(m_start);
	}

	void pause()
	{
		this->stop();
		this->start();
	}
	void stop()
	{
		cudaEventRecord(m_stop);
		cudaEventSynchronize(m_stop);
		cudaEventElapsedTime(&m_time, m_start, m_stop);
		m_total_time += m_time;
		printf("\n GPU elapsed time = %f ms, total time = %f ms\n", m_time, m_total_time);
	} 


};
#endif