#ifndef ___EXCLUSIVESUMPOSITION___
#define ___EXCLUSIVESUMPOSITION___

#include <cuda.h>
#include <cuda_runtime.h>
#include <cub/cub.cuh>
#include <cub/warp/warp_scan.cuh>

#include "Vertex.h"
#include "Triangle.h"

#define NUM_NEIGH				8
#define WARP_SIZE				32

struct DistanceInfo
{
	float 			distance;
	char  			region;
	unsigned int	Tidx;
	
	__device__ friend bool operator<(const DistanceInfo& l, const DistanceInfo& r)
    {
		return (l.distance < r.distance);
	}
	
	__device__ friend bool operator>(const DistanceInfo& l, const DistanceInfo& r)
    {
		return (l.distance > r.distance);
	}
	__device__ friend bool operator==(const DistanceInfo& l, const DistanceInfo& r)
    {
		return (l.distance == r.distance);
	}

	/*
	bool operator < (const DistanceInfo& dinfo)
	{
		return (this->distance < dinfo.distance);
	}
	bool operator > (const DistanceInfo& dinfo)
	{
		return (this->distance > dinfo.distance);
	}
	
	bool operator == (const DistanceInfo& dinfo)
	{
		return (this->distance == dinfo.distance);
	}
	*/
};

__global__ void exclusiveDistInfo(
	unsigned int		num_queries,
	unsigned int	*	d_outExclusive,
	unsigned int	*	d_indices,
	gpuVertex		*	pVerts,
	unsigned int		num_verts,
	gpuVertex		*	d_queries,
	gpuTriangle		*	pfaces,
	float			*	d_outDList,
	unsigned int	*	d_NeighFaces);


#endif