#include "Triangle.h"

__host__ __device__ Vertex * Triangle::v(unsigned int index, Vertex * verts)
{
	unsigned int a = m_ui[index];
	return &verts[a];
}