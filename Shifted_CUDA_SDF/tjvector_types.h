
//****************************************************************************************
//	tjvector_types.h
// Taejung Park (taejung.park at gmail.com)
// Created : September 24 2013
// Modified :
// 
//
// Description : This is a header file to define custom vector types.
//				 
//****************************************************************************************


#ifndef __TJVECTOR_TYPES___H__________
#define __TJVECTOR_TYPES___H__________


namespace tjtypes
{
	struct fvec3
	{
		float x, y, z;

		float& operator[] (const int i);

		float operator[] (const int i) const;

	};

	struct ivec3
	{
		int x,y,z;
		int& operator[] (const int i);
		 int operator[] (const int i) const;	
	};
	
	float fminf(const float a, const float b);
	float fmaxf(const float a, const float b);

	/* lerp */
	float lerp(const float a, const float b, const float t);

	/* bilerp */
	float bilerp(const float x00, const float x10, const float x01, const float x11,
                                         const float u, const float v);

	/* clamp */
	float clamp(const float f, const float a, const float b);



	ivec3 make_ivec3(int x, int y, int z);
	fvec3 make_fvec3(float x, float y, float z);
	float copysignf(const float dst, const float src);
/* min */
	 fvec3 fminf(const fvec3& a, const fvec3& b);
	 float fminf(const fvec3& a);

/* max */
	 fvec3 fmaxf(const fvec3& a, const fvec3& b);
	 float fmaxf(const fvec3& a);

/* add */
	 fvec3 operator+(const fvec3& a, const fvec3& b);
	 fvec3 operator+(const fvec3& a, const float b);
	 fvec3 operator+(const float a, const fvec3& b);
	 void operator+=(fvec3& a, const fvec3& b);

/* subtract */
	 fvec3 operator-(const fvec3& a, const fvec3& b);
	 fvec3 operator-(const fvec3& a, const float b);
	 fvec3 operator-(const float a, const fvec3& b);
	 void operator-=(fvec3& a, const fvec3& b);

/* multiply */
	 fvec3 operator*(const fvec3& a, const fvec3& b);
	 fvec3 operator*(const fvec3& a, const float s);
	 fvec3 operator*(const float s, const fvec3& a);
	 void operator*=(fvec3& a, const fvec3& s);
	 void operator*=(fvec3& a, const float s);

/* divide */
	 fvec3 operator/(const fvec3& a, const fvec3& b);
	 fvec3 operator/(const fvec3& a, const float s);
	 fvec3 operator/(const float s, const fvec3& a);
	 void operator/=(fvec3& a, const float s);

/* lerp */
	 fvec3 lerp(const fvec3& a, const fvec3& b, const float t);

/* bilerp */
	 fvec3 bilerp(const fvec3& x00, const fvec3& x10, const fvec3& x01, const fvec3& x11,
                                          const float u, const float v);

/* clamp */
	 fvec3 clamp(const fvec3& v, const float a, const float b);
	fvec3 clamp(const fvec3& v, const fvec3& a, const fvec3& b);

/* dot product */
	 float dot(const fvec3& a, const fvec3& b);

/* cross product */
	 fvec3 cross(const fvec3& a, const fvec3& b);

/* length */
	 float length(const fvec3& v);

/* normalize */
	 fvec3 normalize(const fvec3& v);

/* floor */
	 fvec3 floor(const fvec3& v);

/* reflect */
	 fvec3 reflect(const fvec3& i, const fvec3& n);

	/* array index operator overloading */
	
	unsigned int numCommonElments(tjtypes::ivec3 a, tjtypes::ivec3 b);

	/* copy array */
	fvec3 * copyarray(fvec3 * source, unsigned int length);


};

#endif

