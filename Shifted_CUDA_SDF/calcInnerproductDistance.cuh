#ifndef _____CALCINNERPRODUCTDISTANCE___CUH___
#define _____CALCINNERPRODUCTDISTANCE___CUH___

#include <cuda.h>
#include <cuda_runtime.h>

#include "Vertex.h"
#include "Triangle.h"
#define NUM_NEIGH				8

__global__ void calcInnerproductDistance(
	gpuVertex		*	d_data, 
	gpuVertex		*	d_queries,
	unsigned int	*	d_indices,
	gpuTriangle		*	d_Faces,
	unsigned int		num_verts,
	unsigned int		num_queries,
	gpuVertex		*	d_vertNorm,
	float			*	d_innerDistance);

#endif