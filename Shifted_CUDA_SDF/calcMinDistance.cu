#include "calcMinDistance.cuh"

__device__ unsigned int lanemask_ltt()
{	
	const unsigned int lane = threadIdx.x & (WARP_SIZE -1);
	return (1 << (lane)) - 1;
}
__device__ unsigned int lanemask_lee()
{
	const unsigned int lane = threadIdx.x & (WARP_SIZE -1);
	return (1 << (lane+1)) - 1;
}

__device__ unsigned int warp_segscan2(bool p, unsigned int hd)
{
	const unsigned int masklt = lanemask_ltt();
	const unsigned int maskle = lanemask_lee();
	hd = (hd | 1) & maskle;
	unsigned int above = __clz(hd) + 1;

	unsigned int segmask = ~((~0U) >> above);
// Perform the scan
	unsigned int b = __ballot(p);
	return __popc(b & masklt & segmask);
}

__global__ void calcMinDistance(
	unsigned int		num_queries,
	unsigned int	*	d_outDIdx,
	float			*	d_outDList,
	unsigned int		hd,
	float			*	d_outSortedDist,
	unsigned int	*	d_outSortedIdx,
	float			*	d_outFinDist,
	unsigned int	*	d_outFinIdx)
{
	const unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
	if(num_queries*NUM_NEIGH <= tid)
		return;

	__shared__ float dist[WARP_SIZE];
	__shared__ unsigned int dataindices[WARP_SIZE];

	unsigned int laneIdx = tid & ( WARP_SIZE -1);
	unsigned int index;
	float fdist;

	dataindices[laneIdx] = d_outDIdx[tid];
	dist[laneIdx] = d_outDList[tid];

	__syncthreads();	
	
	bool b, e;
	unsigned int sift, f, t, d, totalFalses, groupFirstIdx, groupLastIdx;
	groupFirstIdx  = (laneIdx/(NUM_NEIGH))* NUM_NEIGH; 
	groupLastIdx = (1u + laneIdx/(NUM_NEIGH))* NUM_NEIGH - 1u; 

	for(sift =1u; sift != 0u ; sift <<= 1)
	{
		fdist = dist[laneIdx];
		index = dataindices[laneIdx];
	 	b = sift &(* (unsigned int *) &fdist);
		e = !b;		   
		f = warp_segscan2(e,hd);		

		totalFalses =  __shfl(e,groupLastIdx) +  __shfl(f,groupLastIdx);	//	x = __shfl(x, 0); // All the threads read x from laneid 0.

		t = laneIdx - f + totalFalses;
		d = b ? t: f+groupFirstIdx;


		dataindices[d]	=	index;
		dist[d]			=	fdist;
	
		__syncthreads();

	}	

	d_outSortedIdx[tid] = dataindices[laneIdx];
	d_outSortedDist[tid] = dist[laneIdx];
	
	if(laneIdx  ==  groupFirstIdx)
	{// store back to the gmem
		unsigned int indexk = tid / NUM_NEIGH;
		d_outFinIdx[indexk] = dataindices[laneIdx];	
		d_outFinDist[indexk] = dist[laneIdx];	
	}
}