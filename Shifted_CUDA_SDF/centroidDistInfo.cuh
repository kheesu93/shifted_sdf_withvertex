#ifndef _____CENTROID___DIST___
#define _____CENTROID___DIST___

#pragma once
#include <cuda.h>
#include <cuda_runtime.h>

#include "Vertex.h"
#include "Triangle.h"

#define NUM_NEIGH				8

__global__ void centroidDistInfo(
	unsigned int		num_queries,
	unsigned int	*	d_indices,
	float			*	d_dist,
	unsigned int		num_verts,
	unsigned int		num_faces,
	gpuTriangle		*	pfaces,
	float			*	d_outDList,
	gpuVertex		*	d_queries,
	unsigned int	*	d_outDIdx);

#endif