#ifndef _____CALCMINDISTANCE___CUH___
#define _____CALCMINDISTANCE___CUH___

#include <cuda.h>
#include <cuda_runtime.h>
#include <cub/device/device_radix_sort.cuh>
#define NUM_NEIGH				8
#define	WARP_SIZE				32

__global__ void calcMinDistance(
	unsigned int		num_queries,
	unsigned int	*	d_outDIdx,
	float			*	d_outDList,
	unsigned int		hd,
	float			*	d_outSortedDist,
	unsigned int	*	d_outSortedIdx,
	float			*	d_outFinDist,
	unsigned int	*	d_outFinIdx);

#endif