//****************************************************************************************
//	morton64.cuh  -- 
// Taejung Park (taejung.park at gmail.com)
// Created : May 15 2016
// Modified :
// 
//
// Description : 64-bit morton code generator for (x,y,z)
//				 
//****************************************************************************************

#ifndef ___MORTON64_CUH_____________________________
#define ___MORTON64_CUH_____________________________


#define MAXX(a, b) (a >= b ? a : b)
#define MINN(c, d) (c < d ? c : d)
// ----------------- 64bit version --------------------------
#include <cuda_runtime.h>
static __host__ __device__ unsigned long long expandBits64(unsigned long long v)
{

	v =(v * 0x0000000100000001ULL) & 0xFFFF00000000FFFFULL; 
	v =(v * 0x0000000000010001ULL) & 0x00FF0000FF0000FFULL; 
	v =(v * 0x0000000000000101ULL) & 0xF00F00F00F00F00FULL; 
	v =(v * 0x0000000000000011ULL) & 0x30C30C30C30C30C3ULL; 
	v =(v * 0x0000000000000005ULL) & 0x1249249249249249ULL; 	  
	return v;
}

// Calculates a 63-bit Morton code for the
// given 3D point located within the unit cube [0,1].
static __host__ __device__  unsigned long long morton3D64(float x, float y, float z)
{
	x = MINN(MAXX(x * 2097152.0f, 0.0f), 2097151.0f);
	y = MINN(MAXX(y * 2097152.0f, 0.0f), 2097151.0f);
	z = MINN(MAXX(z * 2097152.0f, 0.0f), 2097151.0f);
	unsigned long long xx = expandBits64((unsigned long long)x);
	unsigned long long yy = expandBits64((unsigned long long)y);
	unsigned long long zz = expandBits64((unsigned long long)z);
	return xx * 4 + yy * 2 + zz;
}

#endif
