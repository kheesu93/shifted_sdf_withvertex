/**
 * @file   tjvector_types.cpp
 * @Author Taejung Park (taejung.park at gmail.com)
 * @date   October 1  2013
 * @brief  Custom vector types
 *
 *This is a source file to define custom vector types which are compatible with optix vector types
 */

#include "tjvector_types.h"
#include <math.h>

//using namespace tjtypes;

namespace tjtypes
{
	float& fvec3::operator[] (const int i) 
	{
		float null_val = 0.0f;
		switch(i)
		{
		case 0:
			return (this->x);
			break;
		case 1:
			return (this->y);
			break;
		case 2:
			return (this->z);
			break;			
		}	
		return null_val;
	};
	
	float fvec3::operator[] (const int i) const
	{
		float val;
		switch(i)
		{
		case 0:
			val = this->x ;
			break;
		case 1:
			val = this->y;
			break;
		case 2:
			val  = this->z;
			break;			
		}	
		return val;
	};

	

	
	int& ivec3::operator[] (const int i) 
	{
		int null_val = 0.0f;
		switch(i)
		{
		case 0:
			return (this->x);
			break;
		case 1:
			return (this->y);
			break;
		case 2:
			return (this->z);
			break;			
		}	
		return null_val;
	};

	int ivec3::operator[] (const int i) const
	{
		int val;
		switch(i)
		{
		case 0:
			val = this->x ;
			break;
		case 1:
			val = this->y;
			break;
		case 2:
			val  = this->z;
			break;			
		}	
		return val;
	};	
	
	float fminf(const float a, const float b)
	{
		return a < b ? a : b;
	}

	 float fmaxf(const float a, const float b)
	{
		return a > b ? a : b;
	};

	/* lerp */
	 float lerp(const float a, const float b, const float t)
	{
		return a + t*(b-a);
	};

	/* bilerp */
	 float bilerp(const float x00, const float x10, const float x01, const float x11,
                                         const float u, const float v)
	{
		return lerp( lerp( x00, x10, u ), lerp( x01, x11, u ), v );
	};

	/* clamp */
	 float clamp(const float f, const float a, const float b)
	{
		return fmaxf(a, fminf(f, b));
	};


	 ivec3 make_ivec3(int x, int y, int z)
	{
		ivec3 v;
		v.x = x; v.y = y; v.z = z;
		return v;
	};

	 fvec3 make_fvec3(float x, float y, float z)
	{
		fvec3 v;
		v.x = x; v.y = y; v.z = z;
		return v;
	};

	
	/* copy sign-bit from src value to dst value */
	 float copysignf(const float dst, const float src)
	{
		union 
		{
			float f;
			unsigned int i;
		} v1, v2, v3;
		v1.f = src;
		v2.f = dst;
		v3.i = (v2.i & 0x7fffffff) | (v1.i & 0x80000000);

		return v3.f;
	} ;
/* min */
	fvec3 fminf(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(fminf(a.x,b.x), fminf(a.y,b.y), fminf(a.z,b.z));
	};

	float fminf(const fvec3& a)
	{
		return fminf(fminf(a.x, a.y), a.z);
	};

	/* max */
	fvec3 fmaxf(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(fmaxf(a.x,b.x), fmaxf(a.y,b.y), fmaxf(a.z,b.z));
	};

	float fmaxf(const fvec3& a)
	{
		return fmaxf(fmaxf(a.x, a.y), a.z);
	};

	/* add */
	fvec3 operator+(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(a.x + b.x, a.y + b.y, a.z + b.z);
	};
	fvec3 operator+(const fvec3& a, const float b)
	{
		return make_fvec3(a.x + b, a.y + b, a.z + b);
	};
	fvec3 operator+(const float a, const fvec3& b)
	{
		return make_fvec3(a + b.x, a + b.y, a + b.z);
	};
	void operator+=(fvec3& a, const fvec3& b)
	{
		a.x += b.x; a.y += b.y; a.z += b.z;
	};
	
	/* subtract */
	fvec3 operator-(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(a.x - b.x, a.y - b.y, a.z - b.z);
	};
	fvec3 operator-(const fvec3& a, const float b)
	{
		return make_fvec3(a.x - b, a.y - b, a.z - b);
	};
	fvec3 operator-(const float a, const fvec3& b)
	{
		return make_fvec3(a - b.x, a - b.y, a - b.z);
	};
	void operator-=(fvec3& a, const fvec3& b)
	{
		a.x -= b.x; a.y -= b.y; a.z -= b.z;
	};

	/* multiply */
	fvec3 operator*(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(a.x * b.x, a.y * b.y, a.z * b.z);
	};

	fvec3 operator*(const fvec3& a, const float s)
	{
		return make_fvec3(a.x * s, a.y * s, a.z * s);
	};
	fvec3 operator*(const float s, const fvec3& a)
	{
		return make_fvec3(a.x * s, a.y * s, a.z * s);
	};
	void operator*=(fvec3& a, const fvec3& s)
	{
		a.x *= s.x; a.y *= s.y; a.z *= s.z;
	};
	void operator*=(fvec3& a, const float s)
	{
		a.x *= s; a.y *= s; a.z *= s;
	};
	
	/* divide */
	fvec3 operator/(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(a.x / b.x, a.y / b.y, a.z / b.z);
	};
	fvec3 operator/(const fvec3& a, const float s)
	{
		float inv = 1.0f / s;
		return a * inv;
	};
	fvec3 operator/(const float s, const fvec3& a)
	{
		return make_fvec3( s/a.x, s/a.y, s/a.z );
	};
	void operator/=(fvec3& a, const float s)
	{
		float inv = 1.0f / s;
		a *= inv;
	};

	/* lerp */
	fvec3 lerp(const fvec3& a, const fvec3& b, const float t)
	{
		return a + t*(b-a);
	};
	
/* bilerp */
	fvec3 bilerp(const fvec3& x00, const fvec3& x10, const fvec3& x01, const fvec3& x11,const float u, const float v)
	{
		return lerp( lerp( x00, x10, u ), lerp( x01, x11, u ), v );
	};

/* clamp */
	fvec3 clamp(const fvec3& v, const float a, const float b)
	{
		return make_fvec3(clamp(v.x, a, b), clamp(v.y, a, b), clamp(v.z, a, b));
	};
	
	fvec3 clamp(const fvec3& v, const fvec3& a, const fvec3& b)
	{
		return make_fvec3(clamp(v.x, a.x, b.x), clamp(v.y, a.y, b.y), clamp(v.z, a.z, b.z));
	};

/* dot product */
	float dot(const fvec3& a, const fvec3& b)
	{
		return a.x * b.x + a.y * b.y + a.z * b.z;
	};
	
	/* cross product */
	fvec3 cross(const fvec3& a, const fvec3& b)
	{
		return make_fvec3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
	};

/* length */
	float length(const fvec3& v)
	{
		return sqrtf(dot(v, v));
	};

/* normalize */
	fvec3 normalize(const fvec3& v)
	{
		float invLen = 1.0f / sqrtf(dot(v, v));
		return v * invLen;
	};

/* floor */
	fvec3 floor(const fvec3& v)
	{
		return make_fvec3(::floorf(v.x), ::floorf(v.y), ::floorf(v.z));
	};

/* reflect */
	fvec3 reflect(const fvec3& i, const fvec3& n)
	{
		return i - 2.0f * n * dot(n,i);
	};


	unsigned int numCommonElments(tjtypes::ivec3 a, tjtypes::ivec3 b)
	{
		unsigned int count = 0;
		unsigned int i, j;

		for(i=0; i<3; i++)
		{
			int elem_a = a[i];

			for(j=0;j<3; j++)
			{
				int elem_b = b[j];

				if(elem_a == elem_b)
					count++;
			}
		}
		return count;
	};
		
	fvec3 * copyarray(fvec3 * psource, unsigned int length)
	{
		unsigned int i;
		fvec3 * ptarget = new fvec3 [length];
		for(i = 0 ; i < length ; i++)
			ptarget[i] = psource[i];

		return ptarget;
	};


};