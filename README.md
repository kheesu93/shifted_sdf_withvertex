# shifted_SDF_withVertex

### What is this repository for? ###

* 석사 논문 프로그램 코드
* Implementation of Signed Distance Fields Using GPU-based Parallel Shifted Sort Algorithm
* https://kheesu93@bitbucket.org/kheesu93/shifted_sdf_triangle.git


### Setting Environment ###

* Window 7(64bits)
* NVIDIA GeForce GTX 1080
* CUDA 8.0
* Visual Studio 2012
* Release mode


### Contribution guidelines ###

1. Import the 3D model
2. Read all vertices and triangles from 3D modeling
3. Calculate the normal vector and store the neighbor faces informations (neighbor number, neighbor vertices...)
	* parallel programming : calcFaceNorm(kernel 1), calcVertexNorm(kernel 2)
4. Use Shifted sort algorithm for fast and almost exact calculation
5. Calculate the distance from k datapoints to query point
6. Find Min distance from 5 --> reduction
7. Best distance per query point * number of query points  --> Distance Fields values
8. Calculate the Sign
9. Combine sign and DF values
10. Complete Signed Distance Fields